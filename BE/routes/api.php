<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\BorrowedBookController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\ReturnedBookController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::resources(['books' => BookController::class,
    'categories' => CategoriesController::class,
    'patrons' => PatronController::class,
    'borrowed_books' => BorrowedBookController::class,
    'returned_books' => ReturnedBookController::class
]);
Route::get('borrowed', [BorrowedBookController::class, 'index']);
Route::get('borrowed/{id}', [BorrowedBookController::class, 'show']);
Route::post('borrowed', [BorrowedBookController::class, 'store']);

Route::post('returned', [ReturnedBookController::class, 'store']);
Route::get('returned/{id}', [ReturnedBookController::class, 'show']);
Route::get('returned', [ReturnedBookController::class, 'index']);
