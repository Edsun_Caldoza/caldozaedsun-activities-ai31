<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBorrowedBookRequest;
use App\Models\BorrowedBook;
use App\Models\Patron;
use Illuminate\Http\Request;

class BorrowedBookController extends Controller
{
    //
    public function index(){
        return response()->json(BorrowedBook::with(['patron', 'book', 'book.category'])->get());
    }
    public function show($id){
        return response()->json(BorrowedBook::with(['patron', 'book', 'book.category'])->where('id', $id)->firstOrFail());
    }
    public function store (StoreBorrowedBookRequest $request){
        BorrowedBook::create($request->validated());

        $create_borrowed = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));
        $borrowedbook = BorrowedBook::with(['book'])->find($create_borrowed->id);
        $copies = $borrowedbook->book->copies - $request->copies;
        $borrowedbook->book->update(['copies' => $copies]);
       return response()->json(['message' => 'Book borrowed successfully', 'borrowedbook' => $borrowedbook]);
    }
}
