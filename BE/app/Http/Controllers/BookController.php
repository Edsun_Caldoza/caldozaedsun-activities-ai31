<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBookRequest;
use App\Models\Books;
use Illuminate\Http\Request;

class BookController extends Controller
{
    //
    public function index(){
        return response()->json(Books::with(['category:id', 'category'])->get());
    }
    public function store(StoreBookRequest $request){
        Books::create($request->validated());
        return response()->json(Books::create($request->all()));
    }
    public function update(StoreBookRequest $request, $id){
        $books = Books::with(['category:id category'])->where('id', $id)->firsOrFail();
        $books->update($request->all());
        return response()->json(['book' => $books]);
    }
    public function destroy($id){
        return response()->json(['message', 'Book is Deleted', Books::where('id', $id)->firstOrFail()->delete()]);
    }
}