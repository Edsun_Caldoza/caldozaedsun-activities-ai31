<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use GuzzleHttp\RetryMiddleware;
use Illuminate\Http\Request;
use App\Http\Requests\StoreReturnedBookRequest;

class ReturnedBookController extends Controller
{
    public function index(){
        return response()->json(ReturnedBook::with(['book', 'patron', 'book.category'])->get());
     }
     public function store(StoreReturnedBookRequest $request){
        ReturnedBook::create($request->validated());
        $borrowedbook = BorrowedBook::find($request->book_id);
        Books::where('id', $request->book_id)->update(['copies' => $borrowedbook->copies + $request->copies]);
        ReturnedBook::create($request->all());
        $this->destroy($request->patron_id);
        return response()->json(['message', 'Book Returned']);
     }
     public function show($id){
         return response()->json(ReturnedBook::with(['book', 'book.category', 'patron'])->findOrFail($id));
     }
}
