<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserSettings;

class UserSettingsController extends Controller
{
    public function index(){
        $usersettings = UserSettings::all();
        return response()->json($usersettings);
    }
}
