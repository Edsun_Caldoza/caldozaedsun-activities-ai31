<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePatronRequest;
use App\Models\Patron;
use Illuminate\Http\Request;

class PatronController extends Controller
{
    //
    public function index(){
        return response()->json(Patron::all());
    }
    public function show($id){
        return response()->json(Patron::findorfail($id));
    }
    public function store(StorePatronRequest $request){
        Patron::create($request->validated());
        return response()->json(Patron::create($request->all()));
    }
    public function update(StorePatronRequest $request, $id){
        $patron = Patron::where('id', $id)->firstOrFail();
        $patron->update($request->all());
        return response()->json(['message', 'Patron Updated', 'patron'=>$patron]);
    }
    public function destroy($id){
        return response()->json()->Patron::where('id', $id)->firstOrFail()->delete();
    }
}