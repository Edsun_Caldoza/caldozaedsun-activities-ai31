<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\BorrowedBook;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class StoreReturnedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $borrowed = BorrowedBook::where('book_id', request()->get('book_id'))->where('patron_id', request()->get('patron_id'))->first();
            if (!empty($borrowed)) {
                $copies = $borrowed->copies;
            } else {
                $copies = request()->get('copies');
            }

        return [
            'book_id' => 'required|exists:borrowed_books,book_id',
            'copies' => 'required|numeric',
            'patron_id' => 'exists:borrowed_books,patron_id'
        ];
    }

      /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function message(){
        return [        
            'copies.required' => 'Copies is required.',
            'copies.numeric' => 'Invalid input. Copies must only be a number.',
            'patron_id.exists' => 'Patron does not exist'
        ];
    }

    //Display error message
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}