<?php

namespace Database\Seeders;

use App\Models\Categories;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories=['Fiction', 'Science', 'Novel'];
       
        foreach ($categories as $category) {
            Categories::create(['category'=>$categories]);
       }
    }
}
