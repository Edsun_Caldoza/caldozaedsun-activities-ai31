Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
  type: 'pie',
  data: {
    labels: ["Green", "Black", "Yellow", "Green"],
    datasets: [{
      data: [80, 20, 16, 8],
      backgroundColor: ['#1BB09A', '#1C2127', '#ffc107', '#28a745'],
    }],
  },
});
