import axios from "@/config/axios";


export default {
  namespaced: true,

  state: {
    borrowed: [],
  },
  getters: {
    getBorrowedBooks: (state) => state.borrowed,
  },
  mutations: {
    setBorrowedBook: (state, borrowed) => state.borrowed.push({ ...borrowed }),
    setBorrowedBooks: (state, borrowedBooks) =>
      (state.borrowed = borrowedBooks),
  },
  actions: {
    async borrowedBook({ commit }, borrow) {
      const response = await axios
        .post("/borrowedbooks", borrow)
        .then((res) => {
          commit("setBorrowedBook", res.data.borrowed);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
        return response;
    },
    async fetchBorrowedBooks({ commit }) {
      await axios
        .get("/borrowedbooks")
        .then((res) => {
          commit("setBorrowedBooks", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    },
  },
};