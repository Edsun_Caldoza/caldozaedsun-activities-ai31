import axios from "@/config/axios";

export default {
  namespaced: true,

  state: {
    usersettings: [],
  },
  getters: {
    getUserSettings: (state) => state.usersettings,
  },
  mutations: {
    setUserSettings: (state, usersettings) => (state.usersettings = usersettings),
  },
  actions: {
    async fetchUserSettings({ commit }) {
      await axios
        .get("/settings")
        .then((res) => {
          commit("setUserSettings", res.data);
        })
        .catch((res) => {
          console.log(res);
        });
    },
  },
};