import Vue from "vue";
import Vuex from "vuex";
import books from "./modules/books";
import patrons from "./modules/patrons";
import categories from "./modules/categories";
import borrowed from "./modules/borrowed";
import returned from "./modules/returned";
import usersettings from "./modules/usersettings";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    books,
    patrons,
    categories,
    borrowed,
    returned,
    usersettings
  },
});