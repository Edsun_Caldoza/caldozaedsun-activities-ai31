import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/pages/Dashboard'
import Patron from '@/components/pages/Patron'
import Books from '@/components/pages/Books'
import Settings from '@/components/pages/Settings'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: Dashboard,
            component: Dashboard
        },
        {
            path: '/patron',
            name: Patron,
            component: Patron
        },
        {
            path: '/books',
            name: Books,
            component: Books
        },
        {
            path: '/settings',
            name: Settings,
            component: Settings
        }
    ]
})