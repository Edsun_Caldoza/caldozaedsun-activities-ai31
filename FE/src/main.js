import Vue from 'vue'
import Dashboard from './Index'
// Import Router from router Folder
import router from './components/router/Routes'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueToastr2 from 'vue-toastr-2'


// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
 
window.toastr = require('toastr')
Vue.use(VueToastr2, {
  progressBar: true,
  closeButton: true,
  timeOut: 5000
})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(Dashboard),
}).$mount('#app')